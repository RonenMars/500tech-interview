import React from 'react';

const GridHeaderRow = ({ headersData }) =>
	headersData ? (
		<tr>
			{headersData.map((header) => <th key={header.field}>{header.title}</th>)}
		</tr>
	) : (
		<tr>
			<th />
		</tr>
	);

export default GridHeaderRow;    