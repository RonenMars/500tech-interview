import React from 'react';

// Row component - display columns registered in the config
const GridRow = ({ movieData, headerConfig }) =>
	movieData && headerConfig ? (
		<tr key={movieData.imdbID}>
			{headerConfig.map((perConfig) => (
				<td key={perConfig.field}>{movieData[perConfig.field]}</td>
			))}
		</tr>
	) : (
		<tr />
	);



export default GridRow;    