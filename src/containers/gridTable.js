import React, { Component } from 'react';
import GridRow from '../components/gridRow';
import GridHeaderRow from '../components/gridHeaderRow';

class GridTable extends Component {

  // Filling the table headers row with data
  tableHeaders = () => <GridHeaderRow headersData={this.props.configuration} />;

  // Filling the table content with data
  tableData = () =>
    this.props.moviesData.map((movie) => (
      <GridRow
        key={movie.imdbID}
        movieData={movie}
        headerConfig={this.props.configuration}
      />
    ));

  // Return table structure merged with data
  render() {
    return (
      <table>
        <thead>{this.tableHeaders()}</thead>
        <tbody>{this.tableData()}</tbody>
      </table>
    );
  }

};

export default GridTable;