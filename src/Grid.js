import React from 'react';
import GridTable from './containers/gridTable';

const Grid = ({ config, data }) => (
  <GridTable configuration={config} moviesData={data} />
);

export default Grid;